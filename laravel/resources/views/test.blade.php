<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>

<!-- pass through the CSRF (cross-site request forgery) token -->
<meta name="csrf-token" content="<?php echo csrf_token() ?>" />

<!-- some test buttons -->
<button id="get">Get data</button>
<button id="post">Post data</button>

<script>
    // set up jQuery with the CSRF token, or else post routes will fail
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
    // handlers
    function onGetClick(event)
    {
        // we're not passing any data with the get route, though you can if you want
        $.get('ajax/get', onSuccess);
    }
    function onPostClick(event)
    {
        // we're passing data with the post route, as this is more normal
        $.post('ajax/post', {payload:'hello'}, onSuccess);
    }
    function onSuccess(data, status, xhr)
    {
        // with our success handler, we're just logging the data...
        console.log(data, status, xhr);
        // but you can do something with it if you like - the JSON is deserialised into an object
        console.log(String(data.value).toUpperCase())
    }
    // listeners
    $('button#get').on('click', onGetClick);
    $('button#post').on('click', onPostClick);
</script>