<html>
<head>
    <title>mmm...Potato</title>
    <meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">
    <link rel="stylesheet" href="css/styles.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/startAjax.js"></script>
</head>
<body>
<div class="content">
    <div class="main">
        <header class="toparea">

            <div class="toptext"><img class="icon" src="images/1.png"> &nbsp;&nbsp;Potatoes project</div>
        </header>

        <div class="menu">
            <ul>
                <li>{!!Html::link('/','Home')!!}</li>
                <li>{!!Html::link('/calc','Calculation page')!!}</li>
                <li>{!!Html::link('/about','About')!!}</li>
            </ul>
        </div>

        <div class="aboutheader">
            {!! $header !!}
        </div>
        <div class="aboutcontent">
            {!! $content !!}
        </div>
    </div>
</body>
</html>