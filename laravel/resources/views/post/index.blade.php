
<meta name="csrf_token" content="{{ csrf_token() }}" />
<html>
<head>
    <title>mmm...Potato</title>
    <meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">
    <link rel="stylesheet" href="css/styles.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/startAjax.js"></script>
    <script type="text/javascript" src="http://vk.com/js/api/share.js?92" charset="windows-1251"></script>
    <script type="text/javascript" src="http://vk.com/js/api/share.js?90" charset="windows-1251"></script>

</head>
<body>
<div class="content">
        <header class="toparea">

            <div class="toptext"><img class="icon" src="images/1.png"> &nbsp;&nbsp;Potatoes project</div>
            <div class="vkpublic">
                <script type="text/javascript">
                    document.write(VK.Share.button(false,{type: "custom", text: "<img src=\"http://vk.com/images/share_32_eng.png\" width=\"32\" height=\"32\" />", eng: 1}));
                    </script>
            </div>
        </header>

        <div class="menu">
            <ul>
                <li>{!!Html::link('/','Home')!!}</li>
                <li>{!!Html::link('/calc','Calculation page')!!}</li>
                <li>{!!Html::link('/about','About')!!}</li>
            </ul>

        </div>
        <div class="hdr">Try it!</div>
        <div class="formcalc">

            <br />
            <div class="fc">Enter a product name:<input type="text" name="productname" id="productname"><br /></div>
            <div class="fc">Choose a currency:&nbsp;&nbsp;&nbsp;&nbsp;
                <select name="currency" id="currency">
                    <option name="USD" checked>USD</option>
                    <option name="EUR">EUR</option>
                    <option name="BYR">BYR</option>
                    <option name="RUB">RUB</option>
                </select><br /></div>
            <div class="fc">
                Enter a product cost:&nbsp;&nbsp;<input type="text" name="cost" id="cost"><br /></div>
            <br />
            <div class="fc"><button id="send" class = "send-btn">Calculate</button></div>

        </div>
        <div id="answer" class="answer">
                <h1>Hey! Your answer:</h1>
                <div class="message">
                    <div class="potatos">You need <span id="kg"></span> kg of potato</div>
                    <div id="vkShare"></div>
                </div>
        </div>
        <div class="clear"></div>
</div>
<script type="text/javascript">

    function createVkShareButton() {
        var vkButton = VK.Share.button({
            url: 'http://potatoes_project.com',
            title: 'Calculations in potatoes',
            description: 'And you want to learn how much is ' + $('input[name=productname]').val() + ' in potato currency? Then visit the website!',
            image: 'http://mysite.com/mypic.jpg',
            noparse: true

        });

        $('#vkShare').append(vkButton);
    }

            $(document).ready(function(){
        $('.send-btn').click(function(){
            if($('#productname').val() != '' && $('#cost').val() != '' && +$('#cost').val() != NaN) {

                $.ajax({
                    url: 'ajax',
                    type: "post",
                    beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    data: {
                        'header': $('input[name=productname]').val(),
                        'type': $('select[name=currency]').val(),
                        'content': $('input[name=cost]').val()
                    },
                    success: function (data) {
                        $('#kg').empty();
                        $('#vkShare').empty();
                        $('#kg').append(data);
                        createVkShareButton();
                        $('.answer').css("display", 'block');
                    }
                });
            }

        });
    });

</script>
</body>
</html>