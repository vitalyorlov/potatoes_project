<html>
<head>
    <title>mmm...Potato</title>
    <meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">
    <link rel="stylesheet" href="css/styles.css">
    <script src="js/jquery.min.js"></script>

</head>
<body>
<div class="content">
    <div class="main">
        <header class="toparea">

            <div class="toptext"><img class="icon" src="images/1.png"> &nbsp;&nbsp;Potatoes project</div>
        </header>

        <div class="menu">
            <ul>
                <li>{!!Html::link('/','Home')!!}</li>
                <li>{!!Html::link('/calc','Calculation page')!!}</li>
                <li>{!!Html::link('/about','About')!!}</li>
            </ul>
        </div>

        <div class="hdr">Look the calculations from any users:</div>

        <table>
            <tr>
                <th>id</th>
                <th>product name</th>
                <th>currency</th>
                <th>cost in currency</th>
                <th>cost in potatoes, kg</th>
            </tr>
            @foreach ($calcs as $calc)
                <tr>
                    <td>{{$calc->id}}</td>
                    <td>{{$calc->product_name}}</td>
                    <td>{{$calc->currency}}</td>
                    <td>{{$calc->cost}}</td>
                    @foreach ($currencys as $currency)
                        @if ($currency->type === $calc->currency)
                    <td>{{$calc->cost / $currency->value}}</td>
                        @else

                        @endif
                        @endforeach
                </tr>
            @endforeach
        </table>


    </div>
</body>
</html>