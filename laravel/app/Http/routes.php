<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Support\Facades\DB;
//get('/', ['as' => 'posts', 'uses' => 'PostController@index']);
//Route::get('/about', function() { return view('post.index'); });
post('/ajax','PostController@ajax');

//Route::get('/', function () {
//    return view('post/index');
//});

Route::post('/admin/content',function (){
    // Getting all post data
    if(Request::ajax()) {
        $data = Input::all();

        DB::table('about')
            ->where('id', 1)
            ->update(array('header' => $data["header"],'content' => $data["content"]));

        print_r($data["header"]);die;

    }
});
Route::post('/admin/constants',function (){
    // Getting all post data
    if(Request::ajax()) {
        $data = Input::all();

        DB::table('potato_cost')
            ->where('id', 1)
            ->update(array('cost' => $data["cost"]));

        print_r($data["cost"]);die;

    }
});


Route::get('api/v1/{productname}/{type}/{cost}', 'PostController@callAPI');


Route::post('admin/content','PostController@updateContent');
Route::post('arr', 'PostController@calc');
//post('/admin/context', 'PostController@updateContext');


get('/', ['as' => 'posts', 'uses' => 'PostController@index']);
get('calc', 'PostController@calc');
get('about', 'PostController@about');

//Route::get('/calc', function() { return view('post.index'); });

