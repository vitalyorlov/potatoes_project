<?php

namespace App\Http\Controllers;

use App\Models\Calculations;
use App\Models\getAbout;
use App\Models\Currency;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Psy\Util\Json;


class PostController extends Controller
{
    public function index()
    {

        return view('post.index');
    }

    public function calc()
    {
        $calculations = Calculations::all();
        $currencys = Currency::all();
        return view('post.calc', ['calcs' => $calculations, 'currencys' => $currencys]);
    }

    public function about()
    {
        $about_content = getAbout::find('1');
        return view('post.about', ['header' => $about_content->header, 'content' => $about_content->content]);
    }

    public function ajax(Request $request)
    {
        if($request->ajax()) {
            $data = $request->input('content');
            $currencys =  Currency::all();
            foreach($currencys as $currency)
            {
                if ($currency->type === $request->input('type')) $value = $currency->value;
            }

            DB::insert('insert into calculations (product_name, currency, cost, CRUD) values (?, ?, ?, ?)',
                [$request->input('header'), $request->input('type'), $request->input('content'), 'INSERT']);

            return $data / $value;

        }
    }

    public function callAPI($product_name, $type, $cost) {
        $currencys =  Currency::all();
        foreach($currencys as $currency)
        {
            if ($currency->type === $type) $value = $currency->value;
        }
        return $cost / $value;

    }


    public function updateContent(Request $request){
        if($request->ajax) {
                        $data = Input::all();

                        DB::table('about')
                            ->where('id', 1)
                            ->update(array('header' => $data["header"],'content' => $data["content"]));
        }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
